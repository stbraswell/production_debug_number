import csv
import requests
import json
import os
import datetime
import time
import sys
import glob
import thread
import threading
from threading import Thread
import os.path
import traceback
import re
import collections
import itertools as it
from lxml import html
from lxml.html.soupparser import fromstring
from dateutil import tz
from selenium import webdriver
from selenium.webdriver import ChromeOptions 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import math
import calendar
import unicodedata


date_str = time.strftime("%d-%m-%Y")
vulcanURL='http://vulcan-fake_URL.com'														# Vulcan homepage (external)
mesActivityURL='http://internalAPI.com:8081/mfgapi/plantno/units/%s/history'				# MFG DATA API (internal)

base = os.path.dirname(__file__)
if not base:
	base = os.getcwd()
	
user_dir = os.environ['USERPROFILE']
vulc_folder = base + '/Vulcan_Number_DB'
if not os.path.exists(vulc_folder):		# Create folder if needed
	os.makedirs(vulc_folder)
failureScript = base + '/daily_failure_report.py'
vulcFile = vulc_folder + '/vulc_ts.txt'
vulc_ORG_FAILS = vulc_folder + '/failure_records_all*.csv'
vulcProdFile = vulc_folder + '/vulcProdSummary_test.csv'
vulcCSV_Lastime = vulc_folder + '/vulcCSV_Lastime2.csv'
vulcCSV_Current = vulc_folder + '/vulcCSV_Now.csv'
opErrDB = vulc_folder + '/OpError_DB_%s.csv' %date_str
opErrDB_Full = vulc_folder + '/OpError_Aging_DB.csv'
kgbErrDB = vulc_folder + '/kgbError_DB_%s.csv' %date_str
kgbErrDB_Full = vulc_folder + '/kgbError_Aging_DB.csv'
vulcGraph = vulc_folder + '/vulc_graph.csv'											# CSV File to store Vulcan number and timestamp
todaysFailures = vulc_folder + '/Failures_for-%s.csv' %date_str
pass_folder = vulc_folder + '/Daily_Passes'
if not os.path.exists(pass_folder):		# Create folder if needed
	os.makedirs(pass_folder)
todaysPasses = pass_folder + '/Passes_for-%s.csv' %date_str
snRetest_counter = vulc_folder + '/Serial_Number_Retest_Counter-%s.csv' %date_str
recoveredNumber_file = vulc_folder + '/Recovered_Counter-%s.txt' %date_str
sem = threading.Semaphore(10)
semT = threading.Semaphore(10)
noPastData = 0
summaryNumbers_live = collections.defaultdict(list)
summaryNumbers_old = collections.defaultdict(list)
summaryNumbers_diff_live = collections.defaultdict(list)
newFailSNs = collections.defaultdict(list)
productCounterList=[]
summaryData2Write=[]
histArray=[]
currentSerialList=[]
previousSerialList=[]
retestList = []
notRetestedList = []
outArray = [] #['Product','SN','Test','Fail Text','URL','server','dut','status']
opError = [] #['Product','SN','Test','Fail Text','URL','server','dut','operator']
kgbList = []
opErrorTriggers = ['TestSetupFailure','INVALID SETUP','TEST SETUP ERROR','FAIL: ASY barcode check for']
opErrorFalseTriggers = ['TestSetupFailure: SSD should be 30000 MB',
						'TEST SETUP ERROR: The temperature in the thermal chamber (5.2) is more than 5.0 degrees off the desired temperature for the dut (0)',
						'TestSetupFailure: Could not obtain a root login to the dut',
						'TestSetupFailure: Failed to get root login to the dut due to timeout. Details: Timeout (420 seconds) trying to discover the current mode but got no output',
						'Invalid link status on linecard connectors',
						'--prbsTest True --fanPwm 100 ==>']
jobs=[]
dataReadSearch = '(?:========== BEGIN: Data read before [A-Za-z]* ==========\n)(.*)'#\n)'
testFailSearch = '(##! TEST FAIL: .*)'
cmdFailSearch = '(##! COMMAND FAIL: .*)'
pyException = "(==================== Exception raised in '.*====================)"
fatalError = 'FATAL ERROR:.*'#\n'
devSearch = '\n.*!!! DEV-[0-9]* applied to wrong PCA \([A-Za-z0-9]*\) !!!'
testSetError = 'TEST SETUP ERROR: .*\n'
fvtIerror= 'FAIL:.*'
from_zone = tz.tzutc()
to_zone = tz.tzlocal()
locationDict = collections.defaultdict(list)
noteDict = collections.defaultdict(list)
redo_list=[]
sucess=0

# Write Vulcan Number and Timestamp to CSV (called from getVulcanCSV_selenium)
def listvulcan(string):		#1046 -Oct/08-13:03
    vulcan_number = string.split()[0]
    monthdict = list(enumerate(calendar.month_abbr))
    a = string.split()[1] # date and time in "-Oct/27-hh:min" format
    b = a.split('-')
    c = b[1] # Oct/27"
    time = b[2] #HH:MM
    month_a = c.split('/')[0]
    month = "0"
    for i in monthdict:
        if i[1] == month_a:
            month = i[0]
    day = c.split('/')[1]
    year = str(datetime.datetime.now().year)
    date_format = str(month) + "/" + day + "/" + year[-2:] + " "+ time
    print date_format
    file = open(vulcGraph,'ab')
    writers = csv.writer(file)
    writers.writerow([vulcan_number,date_format])
    file.close()

# Write Vulcan Number and Timestamp to CSV (called from getVulcanCSV_api)	
def listvulcan2(string):		#1046-10/08/2018-13:03
	vulcan_number = string.split('-')[0]
	date = string.split('-')[1]
	time = string.split('-')[2]
	date_format = date + " "+ time
	print date_format
	
	# Write unit count and date to csv for graphing
	file = open(vulcGraph,'ab')
	writers = csv.writer(file)
	writers.writerow([vulcan_number,date_format])	
	file.close()

# Get number of units retested since last update, write operator errors to file, create html to list the test failures for this update only
def getInfo():
	justPassed_list = []
	justPassedInfo_list =[]
	newFails_list = []
	snCounter_dict = collections.defaultdict(list)
	allSerials = []
	# Read in Vulcan CSV from the last update, create previousSerialList: [[SN1,fail time],[SN2,fail time],[SN3,fail time],..]
	if os.path.exists(vulcCSV_Lastime):
		with open(vulcCSV_Lastime, 'rb') as f:
			mycsv_lt = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
			mycsv_lt = list(mycsv_lt)
			for row in mycsv_lt[1:]:		
				previousSerialList.append([row[8],row[3]]) #[SN,PITA URL,productname,failed test]	
			

	# Read in Vulcan CSV from this update, create currentSerialList: [[SN1,URL,Product Name,Failed Test],[SN2,URL,Product Name,Failed Test],[SN3,URL,Product Name,Failed Test],...]
	# IF [SN,fail date] are not in previousSerialList, then add unit to currentSerialList.  This effectivly adds new and retest units only.
	with open(vulcCSV_Current, 'rb') as f:
		mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
		mycsv = list(mycsv)
		for row in mycsv[1:]:	
			if [row[8],row[3]] not in previousSerialList:	
				currentSerialList.append([row[8],row[9],row[5],row[6]]) #[SN,PITA URL,productname,failed test]
				# Add SN to list of new fails
				newFails_list.append(row[8])
			allSerials.append(row[8])
##--------------------------------------------------Info for Daily Fail Email------------------------------------------------------##
	# find which SNs have passed their failed test since the last update
	for item in previousSerialList:
		if item[0] not in allSerials:
			#create list of these SNs
			justPassed_list.append(item[0])
			for row in mycsv_lt[1:]:
				if item[0] ==row[8]:
					justPassedInfo_list.append(row)
		
	# Read in "retest counter" file to dictionary {SN1:count,SN2:count,...}
	if os.path.isfile(snRetest_counter):
		with open(snRetest_counter, 'rb') as f:
			mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
			mycsv = list(mycsv)
		for row in mycsv:
			snCounter_dict[row[0]] = int(row[1])		#{SN1:count,SN2:count,...}
	else:
		pass
	
	# Read in Retest Number
	if os.path.isfile(recoveredNumber_file):
		file = open(recoveredNumber_file, 'rb') 
		recoveredNumber = file.read()
		recoveredNumber = int(recoveredNumber)
		file.close()
	else:	# if it doesnt exist its a new day so reset the counter
		recoveredNumber = 0
		
	# Update the recovered number, remove SN from snCounter_dict and write new recoveredNumber to file
	if justPassed_list:
		# Write units that passed to file
		with open(todaysPasses, 'ab') as f:
			clusterwriter = csv.writer(f)	
			for item in justPassedInfo_list:
				clusterwriter.writerow(item)
		
		print 'We got a Winner!'
		for item in justPassed_list:
			print item
			if item in snCounter_dict:
				print snCounter_dict[item]
				if os.path.isfile(recoveredNumber_file):
					recoveredNumber += snCounter_dict[item]
					print 'Recovered Number: %d' %recoveredNumber
				else:
					print 'recovery file not here'
				del snCounter_dict[item]
			else:	# This means that the failure occured yesterday but the unit passed today
				print 'Not in snCounter'
		file = open(recoveredNumber_file, 'wb') 
		file.write(str(recoveredNumber))
		file.close()
	
	# Add new failures and retest failures to snCounter_dict
	if newFails_list:
		for item in newFails_list:
			if item in snCounter_dict:
				snCounter_dict[item] = (snCounter_dict[item] + 1)
			else:
				snCounter_dict[item] = 1

	# Write "retest counter" file  {SN1:count,SN2:count,...}
	with open(snRetest_counter, 'wb') as f:
		clusterwriter = csv.writer(f)
		for item in snCounter_dict:
			clusterwriter.writerow([item,snCounter_dict[item]])
		
##--------------------------------------------------------------------------------------------------------------------------------##
			
	if currentSerialList:
		totalParts = len(currentSerialList)
		## Create list of jobs to run
		for idx, item in enumerate(currentSerialList):
			remaining = totalParts-(idx+1)
			t = Thread(target=getPitaData, args=(item, )) #([SN,PITA URL,productname],number of remaining searches)
			jobs.append(t)

		## Start the threads
		for j in jobs:
			j.start()

		## Ensure all of the threads have finished
		for j in jobs:
			j.join()
		# Write the Days OpErrors to todays file
		if opError:
			with open(opErrDB, 'ab') as f:
				clusterwriter = csv.writer(f)
				for item in opError:
					clusterwriter.writerow(item[:-1])
			# Add these OpErrors to the full list
			with open(opErrDB_Full, 'ab') as f:
				clusterwriter = csv.writer(f)
				for item in opError:
					clusterwriter.writerow(item)
		if kgbList:
			# Write the Days OpErrors to todays file
			with open(kgbErrDB, 'ab') as f:
				clusterwriter = csv.writer(f)
				for item in kgbList:
					clusterwriter.writerow(item[:-1])
			# Add these OpErrors to the full list
			with open(kgbErrDB_Full, 'ab') as f:
				clusterwriter = csv.writer(f)
				for item in kgbList:
					clusterwriter.writerow(item)
		# Write the failures to todays failure file
		with open(todaysFailures, 'ab') as f:
			clusterwriter = csv.writer(f)	
			for item in outArray:
				clusterwriter.writerow(item[:-1])

		for item in outArray:
			newFailSNs[item[0]].append(item[1:]) # {'product' : [[SN,test,fail,URL,server,dut],[SN,test,fail,URL,server,dut]...],'product' : [[SN,test,fail,URL,server,dut],[SN,test,fail,URL,server,dut]...],...}
			
		temp_html = '<br><b><p style="font-size:15px">NEW FAILS:</b></p>\n\
					<table style="border: 1px solid black ;border-collapse:collapse;">\n\
						<col width="8%">\n\
						<col width="8%">\n\
						<col width="4%">\n\
						<col width="65%">\n\
						<col width="3%">\n\
						<col width="12%">\n\
						<col width="12%">\n\
						<tr style="background-color: #CFCFA3;">\n\
							<th style="border: 1px solid black ;">Product</th>\n\
							<th style="border: 1px solid black ;">Serial Number</th>\n\
							<th style="border: 1px solid black ;">Failed Test</th>\n\
							<th style="border: 1px solid black ;">Failure</th>\n\
							<th style="border: 1px solid black ;">Test Server</th>\n\
							<th style="border: 1px solid black ;">DUT		</th>\n\
							<th style="border: 1px solid black ;">Status</th>\n\
						</tr>'
		color_switch=0
		for item in sorted(newFailSNs, key=lambda k: len(newFailSNs[k]), reverse=True):
		#'product' : [[SN,test,fail,URL,server,dut],[SN,test,fail,URL,server,dut]...]
			rowsToSpan=len(newFailSNs[item])
			if color_switch == 0:
				temp_html = temp_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
			else:
				temp_html = temp_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
			for idx, row in enumerate(newFailSNs[item]):	#[SN,test,fail,URL]
				mysn=row[0]
				if idx == 0:
					if color_switch == 0:
						temp_html = temp_html + '<td style="background-color: #FFFFDB;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[3],row[0])
					else:
						temp_html = temp_html + '<td style="background-color: #CFCFA3;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[3],row[0])
				else:
					if color_switch == 0:
						temp_html = temp_html + '<tr style="background-color: #FFFFDB;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[3],row[0])
					else:
						temp_html = temp_html + '<tr style="background-color: #CFCFA3;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[3],row[0])
				for indx, ele in enumerate(row[1:7]):
					if indx != 2:
						try:
							temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % ele.decode('UTF-8').encode('UTF-8')
						except UnicodeEncodeError:
							tb = sys.exc_info()[2]
							tbinfo = traceback.format_tb(tb)[0]
							# # Concatenate information together concerning the error into a message string
							pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
							# Capture the SNs where this function failed
							print pymsg
						except:
							tb = sys.exc_info()[2]
							tbinfo = traceback.format_tb(tb)[0]
							# # Concatenate information together concerning the error into a message string
							pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
							# Capture the SNs where this function failed
							print pymsg
							print mysn
							print type(ele)
				temp_html = temp_html + "</tr>\n"
			if color_switch == 0:
				color_switch += 1
			else:
				color_switch -= 1
		temp_html = temp_html + "</table>\n" 
		return temp_html

def getDUT(driver,indx):
	driver.find_element_by_xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]/a' % (indx+1)).click()
	time.sleep(0.25)
	# Grab html source code
	html_source_dut = driver.page_source
	tree_dut = html.fromstring(html_source_dut)
	dut = tree_dut.xpath('/html/body/table/tbody/tr/td[1]')
	# The html in this section is horrible, so load everything into a list and loop over it until you find the dut and server
	words = [s.strip().split(': ') for s in dut[0].text_content().splitlines()]
	for item in words:
		if item[0] in ['Cell']:
			dut = item[1]
		if item[0] in ['Test Server']:
			server = item[1].split('.')[0]
	return dut,server	
	
# Get Information about the latest test failure per unit
def getPitaData(serialNumber):	#([SN,PITA URL,productname,failed test])
	try:
		serial = serialNumber[0]
		unitURL = serialNumber[1]
		productname = serialNumber[2]
		failedTest = serialNumber[3]
		# Check out thread from manager
		sem.acquire()
		
		running_flag=''
		
		# Chrome Stuff
		chromedriver_path = "C:/Python27/chromedriver_2.35.exe"
		chrome_options = Options() 
		chrome_options.add_argument("--headless")		#HEADLESS!
		chrome_options.add_argument("--disable-gpu")	#HEADLESS!
		chrome_options.add_argument("--log-level=3")
		chrome_options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
		mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
		
		# Open the URL
		mydriver.get(unitURL)
		# Tells the program to wait for 10 seconds before throwing an exception
		mydriver.implicitly_wait(10)
		# Used for explicit waits, program will wait for 30 seconds before throwing exception, while continuously looking for an element
		wait = WebDriverWait(mydriver, 30)
		# Make sure table has loaded
		wait.until(EC.presence_of_element_located((By.XPATH,'/html/body/div[2]/div[1]/table/tbody/tr[1]/td[1]')))
		time.sleep(1)
		#Grab html code for lxml
		html_source = mydriver.page_source
		tree = html.fromstring(html_source)
		# Number of rows  in the test/notes section
		numOfElements = len(tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr'))
		# Put each row into list as an element
		elementArray = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr')
		
		# Start at the bottom of the history and work up
		for idx, item in reversed(list(enumerate(elementArray))):
			# Grab the test name and result elements (column 1 & 2)
			tname = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]' % (idx+1))
			result = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[2]' % (idx+1))
			# Store the text of the test name and result elements
			testName = tname[0].text_content()
			testResult = result[0].text_content()
			
			# If we reach an entry where the failed test has passed, before we hit a failure, then we are done, no need to continue
			if testResult in ['passed'] and (testName == failedTest):
				print 'PASSED: %s' % serial
				break
			if testResult in ['running']:
				print 'running: %s' % testName
				running_flag ='Running: %s' % testName
				continue
			# If we have reached the last FAIL, lets check it out
			if (testResult in ['failed','invalid','bug']) and (testName == failedTest):
				if not running_flag:		# flag unit as failed
					running_flag = 'Failed'
				operator_name = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[4]/text()' % (idx+1))			# Get operator name
				if tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[5]/a/i/text()' % (idx+1)):					# Check if Fail text is listed
					failText = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[5]/a/i/text()' % (idx+1))			# Get Failure Text
					print failText[0] 																						# this will be just a string
					mydriver.find_element_by_xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]/a' % (idx+1)).click()	# Click into Failed Test
					logLink = wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/table/tbody/tr/td[1]/a[2]")))	# Grab the link to logs
					
					# Grab html source code
					html_source_dut = mydriver.page_source
					tree_dut = html.fromstring(html_source_dut)
					
					# The html in this section is horrible, so load everything into a list and loop over it until you find the dut and server
					dut = tree_dut.xpath('/html/body/table/tbody/tr/td[1]')							# Grab all of the text
					words = [s.strip().split(': ') for s in dut[0].text_content().splitlines()]		# Turn it into a list
					for item in words:																# Loop over the list to look for specific info
						if item[0] in ['Cell']:														# Find dut (i.e.:  'Cell: dut26' )
							dut = item[1]
						if item[0] in ['Test Server']:												# Find the Test Server (i.e.: Test Server: at24.xxx.yyy.networks.com)
							server = item[1].split('.')[0]											# Strip out .xxx.yyy.....)
					# Add all info to a list that will be used to populate the final csv
					outArray.append([productname,serial,failedTest,failText[0].strip(),unitURL,server,dut,running_flag])
					
					# Grab KGB errors, add to list that will be used to populate KGB failures
					if 'KGB' in failText[0].strip():
						kgbList.append([productname,serial,failedTest,failText[0].strip(),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
					
					# Grab Operator Errors, add to list that will be used to populate Op Errors
					elif (any(word in failText[0].strip() for word in opErrorTriggers)) and (not any(word in failText[0].strip() for word in opErrorFalseTriggers)):
						opError.append([productname,serial,failedTest,failText[0].strip(),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
					
						
					break
				else:	# If failure info was not on the history page
					try:
						# Click into fail/invalid link
						mydriver.find_element_by_xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]/a' % (idx+1)).click()
					except:
						f = open('failed.txt', 'wb')
						f.write(mydriver.page_source)
						f.close()
						print 'yup failed: %s \n %s' % (serialNumber, idx)
						tb = sys.exc_info()[2]
						tbinfo = traceback.format_tb(tb)[0]
						# Concatenate information together concerning the error into a message string
						pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
						# Capture the SNs where this function failed
						print "%s \n %s" % (pymsg, serial)
					
					# Make sure the link to the log has loaded
					logLink = wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/table/tbody/tr/td[1]/a[2]")))
					# Grab html source code
					html_source = mydriver.page_source
					tree = html.fromstring(html_source)
					# The html in this section is horrible, so load everything into a list and loop over it until you find the dut and server
					dut = tree.xpath('/html/body/table/tbody/tr/td[1]')								# Grab all of the text
					words = [s.strip().split(': ') for s in dut[0].text_content().splitlines()]		# Turn it into a list
					for item in words:																# Loop over the list to look for specific info
						if item[0] in ['Cell']:														# Find dut (i.e.:  'Cell: dut26' )
							dut = item[1]
						if item[0] in ['Test Server']:												# Find the Test Server (i.e.: Test Server: at24.xxx.yyy.networks.com)
							server = item[1].split('.')[0]											# Strip out .xxx.yyy.....)
					listOFails=[]

					## If fail text exists on FAIL Page
					# Different fail types generate different html structures, so we have to check
					if tree.xpath('/html/body/ul/li'):						# Multiple failures are listed
						numOfFails = len(tree.xpath('/html/body/ul/li'))	# Count the number of failures
						failCount=1
						while failCount <= numOfFails:
							# Add fail text to list (ALWAYS ENCODE IT!!)
							failTextShort = tree.xpath('/html/body/ul/li[%s]' % failCount)
							listOFails.append(failTextShort[0].text_content().strip().encode('utf-8'))
							failCount+=1
							# If the fail is listed as Unknown, we have to parse the actual log file...
							if (''.join(listOFails) == 'Unknown'):
								logLink.click()																# Click into Log File
								wait.until(EC.presence_of_element_located((By.XPATH,'/html/body/pre')))		# Wait until the page is loaded
								# Grab html source code
								html_source = mydriver.page_source
								tree = html.fromstring(html_source)
								# Grab the text of the log, and make it readable (ENCODE!)
								logText = tree.xpath('/html/body/pre')
								logText=logText[0].text_content()
								logText = logText.encode('utf-8')
								
								
								# Failures to search log file for
								'''
								dataReadSearch = '(?:========== BEGIN: Data read before [A-Za-z]* ==========\n)(.*)'#\n)'
								testFailSearch = '(##! TEST FAIL: .*)'
								cmdFailSearch = '(##! COMMAND FAIL: .*)'
								pyException = "(==================== Exception raised in '.*====================)"
								fatalError = 'FATAL ERROR:.*'#\n'
								devSearch = '\n.*!!! DEV-[0-9]* applied to wrong PCA \([A-Za-z0-9]*\) !!!'
								testSetError = 'TEST SETUP ERROR: .*\n'
								fvtIerror= 'FAIL:.*'
								'''
								
								# Check for specific test/failure combos
								if testName in ['fvt-interactive','fst-interactive']:
									if re.search(fvtIerror,logText):
										find = re.search(fvtIerror,logText).group().encode('utf-8')
										# Add all info to a list that will be used to populate the final csv
										outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
										print find

										if 'KGB' in find:
											kgbList.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']

										elif (any(word in find for word in opErrorTriggers)) and (not any(word in find for word in opErrorFalseTriggers)):
											opError.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
					
									else:
										print 'LED Failure'
										find = 'LED Failure'
										# Add all info to a list that will be used to populate the final csv
										outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
						
										if 'KGB' in find:
											kgbList.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']

										if (any(word in find for word in opErrorTriggers)) and (not any(word in find for word in opErrorFalseTriggers)):
											opError.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
	
									break
								# Search Logfile for specific fail text
								
								if re.search(dataReadSearch,logText):
									find = re.search(dataReadSearch,logText)
									find = ''.join(find.groups())
									find = "%s" %find
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find
									
								elif re.search(testFailSearch,logText):
									find = re.search(testFailSearch,logText).group().encode('utf-8')
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find

								elif re.search(pyException,logText):
									find = re.search(pyException,logText).group().encode('utf-8')
									find = "'%s" %find
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find
									
								elif re.search(cmdFailSearch,logText):
									find = re.search(cmdFailSearch,logText).group().encode('utf-8')
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find
									
								elif re.search(fatalError,logText):
									find = re.search(fatalError,logText).group().encode('utf-8')
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find

								elif re.search(devSearch,logText):
									find = re.search(devSearch,logText).group().encode('utf-8')
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find
									
								elif re.search(testSetError,logText):
									find = re.search(testSetError,logText).group().encode('utf-8')
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print find
									
								else:
									find = 'UNKNOWN: Check logfile manually'
									# Add all info to a list that will be used to populate the final csv
									outArray.append([productname,serial,failedTest,find,unitURL,server,dut,running_flag])
									print 'Unknown...'
						
								if 'KGB' in find:
									kgbList.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']

								if (any(word in find for word in opErrorTriggers)) and (not any(word in find for word in opErrorFalseTriggers)):
									opError.append([productname,serial,failedTest,find,unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
										
							else:
								# Add all info to a list that will be used to populate the final csv
								outArray.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,running_flag])
								print 'found fail in fail page'

								if 'KGB' in ''.join(listOFails):
									kgbList.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
									
								if (any(word in ''.join(listOFails) for word in opErrorTriggers)) and (not any(word in ''.join(listOFails) for word in opErrorFalseTriggers)):
									opError.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']
										
						break

					elif tree.xpath('/html/body/ol'):
						numOfFails = len(tree.xpath('/html/body/ol/li'))
						failCount=1
						while failCount <= numOfFails:
							failTextShort = tree.xpath('/html/body/ol/li[%s]' % failCount)
							listOFails.append(failTextShort[0].text_content().encode('utf-8'))
							failCount+=1
						# Add all info to a list that will be used to populate the final csv
						outArray.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,running_flag])
						print 'found fail in fail page'
												
						if 'KGB' in ''.join(listOFails):
							kgbList.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']

						elif (any(word in ''.join(listOFails) for word in opErrorTriggers)) and (not any(word in ''.join(listOFails) for word in opErrorFalseTriggers)):
							opError.append([productname,serial,failedTest,''.join(listOFails),unitURL,server,dut,operator_name[0].strip(),date_str])	#['Product','SN','Test','Fail Text','URL','server','dut','operator']

						break

		# Close the window
		mydriver.quit()
		# Release the thread back to the manager
		sem.release()

	except:
		# # Get the traceback object
 		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# # Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		# Capture the SNs where this function failed
 		print "%s \n %s" % (pymsg, serial)
 		# # Close the Browser Window
 		mydriver.quit()
		# Release the thread back to the manager
		sem.release()

# Check if Vulcan has updated via Selenium, Download product CSV, via Selenium, once it has updated
def getVulcanCSV_selenium():
	try:
		
		chromedriver_path = "C:/Python27/chromedriver_2.35.exe"
		chrome_options = Options() 
		chrome_options.add_argument("--log-level=3")
		prefs = {"download.default_directory" : vulc_folder}
		chrome_options.add_experimental_option("prefs",prefs)
		chrome_options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
		mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
		
		# Open the URL
		mydriver.get(vulcanURL)
		
		# Max Window
		mydriver.maximize_window()
		
		# Tells the program to wait for 10 seconds before throwing an exception
		mydriver.implicitly_wait(10)
		
		# Used for explicit waits, program will wait for 30 seconds before throwing exception, while continuously looking for an element
		wait = WebDriverWait(mydriver, 120)
		
		# Make sure table has loaded
		wait.until(EC.presence_of_element_located((By.XPATH,'//*[@id="current-failures-summary"]/table/tfoot/tr/th[2]')))
		time.sleep(1)
		
		#Grab html code for lxml
		html_source = mydriver.page_source
		tree = html.fromstring(html_source)
		
		# Grab the last updated text
		vulcanNumber = tree.xpath('//*[@id="current-failures-summary"]/table/tfoot/tr/th[2]/text()')
		lastUpdateText = tree.xpath('//*[@id="current-failures-summary"]/h6/text()')
		lastUpdate_Date = lastUpdateText[0].split()[1]
		lastUpdate_Time = lastUpdateText[0].split()[2]
		
		# Check if it has changed, if not then wait for it to change
		try:
			file = open(vulcFile, 'rb') 													# Read in timestamp of last update
			oldVulc_ts = file.read()
			newVulc_ts= '%s-%s-%s' % (vulcanNumber[0],lastUpdate_Date,lastUpdate_Time)		# Create string timestamp of current timestamp in Vulcan
			while oldVulc_ts == newVulc_ts:													# If the timestamps are the same, Vulcan hasn't updated yet.  Wait 30 seconds, refresh and check again.
				print 'sleeping'
				mydriver.refresh()
				time.sleep(30)
				html_source = mydriver.page_source
				tree = html.fromstring(html_source)
				vulcanNumber = tree.xpath('//*[@id="current-failures-summary"]/table/tfoot/tr/th[2]/text()')
				lastUpdateText = tree.xpath('//*[@id="current-failures-summary"]/h6/text()')
				lastUpdate_Date = lastUpdateText[0].split()[1]
				lastUpdate_Time = lastUpdateText[0].split()[2]
				file = open(vulcFile, 'rb') 
				oldVulc_ts = file.read()
				newVulc_ts= '%s-%s-%s' % (vulcanNumber[0],lastUpdate_Date,lastUpdate_Time)
				print 'old: %s new: %s' % (oldVulc_ts, newVulc_ts)
		except:
			print 'creating %s' % vulcFile
		
		#write new time stamp to file
		file = open(vulcFile,'wb') 
		file.write('%s-%s-%s' % (vulcanNumber[0],lastUpdate_Date,lastUpdate_Time)) 
		file.close()
		listvulcan('%s-%s-%s' % (vulcanNumber[0],lastUpdate_Date,lastUpdate_Time))
		# get to downloading
		allLink = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="vis_area"]/div/div/div[2]/div[1]/div/div/button[1]')))
		allLink.click()
		csvLink = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="vis_area"]/div/div/div[2]/div[1]/div/div/ul/li[2]/a')))
		# Download file
		csvLink.click()
		time.sleep(6)
		try:
			dlfile = max(glob.iglob( vulc_folder + '/failure_records_all.csv.crdownload'), key=os.path.getctime)
			while os.path.isfile(dlfile):
				print 'Still Downloading'
				time.sleep(5)
		except:
			print 'Done Downloading Detail file'
		## DL Failure CSV

		## if there is data from 2 runs ago, remove it
		if os.path.exists(vulcCSV_Lastime):
			print 'Path Exists (Deleting): %s' % vulcCSV_Lastime
			os.remove(vulcCSV_Lastime)
			time.sleep(3) # Give it time to purge the file before renaming
		## if there is data from last run, rename it to _lasttime
		if os.path.exists(vulcCSV_Current):
			print 'Path Exists (Renaming): %s to %s' % (vulcCSV_Current, vulcCSV_Lastime)
			os.rename(vulcCSV_Current, vulcCSV_Lastime )
		else:
			noPastData = 1
		
		
		file = max(glob.iglob(vulc_ORG_FAILS), key=os.path.getctime)
		## Rename this run to _latest
		print 'renaming downloaded file'
		os.rename(file, vulcCSV_Current )
		
		mydriver.quit()
		# Release the thread back to the manager

	except:
		# # Get the traceback object
 		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# # Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		# Capture the SNs where this function failed
 		print pymsg
 		# # Close the Browser Window
 		mydriver.quit()

# Check if Vulcan has updated via API, Download product CSV, via API, once it has updated
def getVulcanCSV_api(): # check for Vulcan update and dl csv without Selenium
	global sucess
	vulcanFailSumJson = 'http://dist2.xxx.yyy.networks.com:45676/getFailureSummary'
	vulcanCSVLink = 'http://dist2.xxx.yyy.networks.com:45676/getProductFailuresCsv/all'
	jsonInfo = requests.get(vulcanFailSumJson, timeout=30)
	json_data = jsonInfo.json()
	vulcanNumber = json_data['Total']
	lastUpdate = json_data['TimeStamp']
	ts = datetime.datetime.strptime(lastUpdate[:16],'%Y-%m-%dT%H:%M').replace(tzinfo=from_zone)
	ts = ts.astimezone(to_zone)
	ts = ts.replace(tzinfo=None)
	lastUpdate_Date = ts.date().strftime("%m/%d/%Y")
	lastUpdate_Time=ts.time().strftime("%H:%M")
	newVulc_ts= '%s-%s' % (vulcanNumber,lastUpdate)
	try:
		file = open(vulcFile, 'rb') 
		oldVulc_ts = file.read()
		newVulc_ts= '%s-%s' % (vulcanNumber,lastUpdate)
		while oldVulc_ts == newVulc_ts:
			print 'sleeping'
			time.sleep(30)
			jsonInfo = requests.get(vulcanFailSumJson, timeout=30)
			json_data = jsonInfo.json()
			vulcanNumber = json_data['Total']
			lastUpdate = json_data['TimeStamp']
			newVulc_ts= '%s-%s' % (vulcanNumber,lastUpdate)
			print 'old: %s new: %s' % (oldVulc_ts, newVulc_ts)
	except:
		print 'creating %s' % vulcFile
		
	# write new time stamp to file
	file = open(vulcFile,'wb') 
	file.write(newVulc_ts) 
	file.close()
	listvulcan2('%s-%s-%s' % (vulcanNumber,lastUpdate_Date,lastUpdate_Time))	#1046-10/08/2018-13:03
	# Get the csv contents
	r = requests.get(vulcanCSVLink, timeout=30)
		
	## if there is data from 2 runs ago, remove it
	if os.path.exists(vulcCSV_Lastime):
		print 'Path Exists (Deleting): %s' % vulcCSV_Lastime
		os.remove(vulcCSV_Lastime)
		time.sleep(3) # Give it time to purge the file before renaming
	## if there is data from last run, rename it to _lasttime
	if os.path.exists(vulcCSV_Current):
		print 'Path Exists (Renaming): %s to %s' % (vulcCSV_Current, vulcCSV_Lastime)
		os.rename(vulcCSV_Current, vulcCSV_Lastime )
	else:
		noPastData = 1
		
	with open(vulcCSV_Current, 'wb') as f:
		f.write(r.content)
	sucess = 1

# Compare New csv with data from last update, Start html generation
def getVulcNum():
	# Read in Current CSV
	with open(vulcCSV_Current, 'rb') as f:
		mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
		mycsv = list(mycsv)
		
	# Vulcan Number is the number of rows in the csv
	vulcanNumber = (len(mycsv)-1)
	
	# Get the count of units for each product
	# Add the product name for each row into a list
	for row in mycsv[1:]:
		productCounterList.append(row[5])
		
	# Create a collections counter dictionary (product name is the key) {Product1:4, Product2:10,...}
	summaryNumbers_live=collections.Counter(productCounterList)
	
	# Turn Collections counter dictionary into a list for writing 
	for item in summaryNumbers_live:
		summaryData2Write.append([item,summaryNumbers_live[item]])
		
	# Read in product count from last update
	try:
		with open(vulcProdFile, 'rb') as f:
			mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
			mycsv = list(mycsv)
		for row in mycsv:
			summaryNumbers_old[row[0]].append(row[1])
	except IOError:
		print "No previous run info found"

	# Get the differences in product numbers from last update to this update, store results in summaryNumbers_diff_live
	for item in summaryNumbers_live:
		if item in summaryNumbers_old:
			try:
				differ = summaryNumbers_live[item] - int(summaryNumbers_old[item][0])  
			except:
				print type(summaryNumbers_live[item])
				print type(summaryNumbers_old[item][0])
			summaryNumbers_diff_live[item].append(differ)	
		else:
			summaryNumbers_diff_live[item].append(summaryNumbers_live[item]) 
	
	for item in summaryNumbers_old:
		if item not in summaryNumbers_live:
			summaryNumbers_diff_live[item].append(int(summaryNumbers_old[item][0])*-1)
	
	# Create first bit of email html
	theBAD=''
	theGOOD=''
	
	body='<html>\n\
		Hi All,<br>\n\
		<br>\n\
		<div>\n'
	body = body+ '<p>Vulcan is currently at: <b>%s Units</b><br></p>\n' % vulcanNumber #[0]
		
	body = body+'<p style="font-size:15px"><b>Test Engineers and Test Leads,<br>\n\
		Please check on all failed units, find out what went wrong and see if we can recover them.</b><br></p>\n\
		</div>\n\
		<br><br>Thanks,<br>\n\
		Troy<br>\n\
		<br>\n\
		<table style="border: 2px solid black ;border-collapse:collapse;">\n\
		<tr>\n\
		<th style="text-align: left;border: 2px solid black ;">Product</th>\n\
		<th style="border: 2px solid black ;">Change</th>\n\
		</tr>\n'
	
	# Product Number difference Table [theGOOD = # went down | theBAD = # went up]
	for idx,item in enumerate(sorted(summaryNumbers_diff_live.values())):
		prodName= sorted(summaryNumbers_diff_live, key=summaryNumbers_diff_live.get)[idx]
		if item[0] > 0:
			theBAD = theBAD + '<tr>\n\
						<td style="background-color: #f44242;border: 1px solid black ;">%s</td>\n\
						<td style="text-align: center;background-color: #f44242;border: 1px solid black ;">%s</td>\n\
						</tr>\n' %(prodName,item[0])
		elif item[0] < 0:
			theGOOD= theGOOD + '<tr>\n\
						<td style="background-color: #00ff00;border: 1px solid black ;">%s</td>\n\
						<td style="text-align: center;background-color: #00ff00;border: 1px solid black ;">%s</td>\n\
						</tr>\n' %(prodName,item[0])
						
	if (theGOOD) or (theBAD):
		body = body + theGOOD + theBAD + '</table>\n'
	else:
		body = body + '</table> NO CHANGE</html>\n'

	# Write the Product numbers to file for use in next update
	with open(vulcProdFile , 'wb') as f:
		clusterwriter = csv.writer(f)
		for item in summaryData2Write:
			clusterwriter.writerow(item)

	if not theBAD:
		noBAD=1
	else:
		noBAD=0
		
	return vulcanNumber, body, noBAD		

# Send Email
def emailit(num,body):
	fromaddr = "troy.braswell@example.com"
	toaddr = ["troy.braswell@example.com"]
	pfile = os.path.join(user_dir,'pfile.txt')	
	pfile2 = os.path.join(user_dir,'pfile2.txt')
	msg = MIMEMultipart('alternative')
	subject= 'Vulcan #: %s' % num
	msg['From'] = fromaddr
	msg['To'] = ", ".join(toaddr) #toaddr
	msg['Subject'] = subject
	# Get password (uses 2 files to workaround password update delay..)
	file = open(pfile, 'rb') 
	pword= file.read() 
	file.close()
	#try:
	file = open(pfile2, 'rb') 
	pword2= file.read() 
	file.close()
	#except: ADD ERROR TYPE
		#pass
	
	text = "You found Waldo!  Great Job! -Troy"
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(body, 'html')
	msg.attach(part1)
	msg.attach(part2)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	try:
		server.login(fromaddr, pword)
	except:# SMTPAuthenticationError:
		print 'using pfile2'
		server.login(fromaddr, pword2)
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()

#removes all control characters from a string
def remove_control_characters(s):	
    return "".join(ch for ch in s if unicodedata.category(ch)[0]!="C")

# Get test location for unit and notes for that failure
def getLoc(serial,link,failedTest,idx):
	semT.acquire()
	global redo_list
	latency = 0
	try:
		start_timer = time.time()
		jsonInfo = requests.get(mesActivityURL % serial, timeout=30)
		json_data = jsonInfo.json()
		
	except: #ValueError
		tb = sys.exc_info()[2]
		tbinfo = traceback.format_tb(tb)[0]
		# # Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		# Capture the SNs where this function failed
		print serial
		print pymsg
		semT.release()
		return
	latency = time.time() - start_timer
	if latency >= 5:
		print 'Latency=%s' %latency
	location = ''
	if json_data['message'] in ['']:
		try:
			location = json_data['data']['serial']['current_location']
		except:
			tb = sys.exc_info()[2]
			tbinfo = traceback.format_tb(tb)[0]
			# # Concatenate information together concerning the error into a message string
			pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
			# Capture the SNs where this function failed
			print serial
			print pymsg
			locationDict[serial] = location

		locationDict[serial] = location
		
		# Get notes from Operators
		
		try:
			r = requests.get(link)
		except:
			print("Connection refused by the server..")
			print("Let me sleep for 2 seconds")
			print("ZZzzzz...")
			time.sleep(2)
			print("Was a nice sleep, now let me continue...")
			try:
				r = requests.get(link)
			except:
				print("Connection refused by the server..")
				#print(r.status_code)
				print("Adding to redo list")
				t = Thread(target=getLoc, args=(serial,link,failedTest,idx, ))
				redo_list.append(t)
				semT.release()
		try:
			tree = fromstring(r.content)
		except ValueError:	# Sometimes the page will have control characters in it and will throw a ValueError, if this happens, strip out the control characters before parsing.
			pre_tree = remove_control_characters((unicode(r.content, 'utf-8')))
			tree = html.fromstring(pre_tree)

		elementArray = tree.xpath('/html/body/div[2]/div[1]/table/tr')
		testarray = tree.xpath('/html/body/div[2]/div')
		for item in testarray:
			if item[0].text_content() == failedTest:
				if len(item[1]) > 1:
					if item[1][-2][2].text_content().strip() in ['failed','invalid','bug']:
						retestList.append(serial)
					else:
						notRetestedList.append(serial)
						print 'Not retested - %s' %serial

		noteCount = 0	# Count the number of notes leading up to the failed/invalid/bug test.  If it is a different test, then reset the note count to 0
		retestcount = 0	
		for idx, item in reversed(list(enumerate(elementArray))):
			tname = tree.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[1]/a' % (idx+1))
			result = tree.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[2]' % (idx+1))
			# Store the text of the result element
			testName = tname[0].text_content()
			testResult = result[0].text_content()
			if 'note' in testName:
				noteCount += 1
				continue
			# If we have reached the last passed test, we are done
			if testResult in ['failed','invalid','bug'] and (testName == failedTest):# and ((idx +1) != len(elementArray)):
				if noteCount > 0:
					myNote = tree.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[4]/a/i/text()' % (idx+1+noteCount))
										#/html/body/div[2]/div[1]/table/tbody/tr[21]/td[4]/a/i/text()
					try:
						noteDict[serial] = myNote[0].encode('utf-8')
					except:
						tb = sys.exc_info()[2]
						tbinfo = traceback.format_tb(tb)[0]
						# # Concatenate information together concerning the error into a message string
						pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
						# Capture the SNs where this function failed
						print '%s \n %s' %(pymsg,link)
						print myNote
						
					break

				else:
					noteDict[serial] = ''
					break
			noteCount = 0	
	semT.release()

# Create html tables for units that have not been retested in under 2 days since test fail and units that that havent been retested in more than 2 days since test fail 
def failureTracker():
	global redo_list
	location_jobs=[]
	print 'Creating Tracker'
	rightNow = datetime.datetime.now()
	recentNoTest_list = collections.defaultdict(list)
	agingNoTest_list = collections.defaultdict(list)
	# get failure timestamps
	with open(vulcCSV_Current, 'rb') as f:
		mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
		mycsv = list(mycsv)
	for idx,row in enumerate(mycsv[1:]):
		t = Thread(target=getLoc, args=(row[8],row[9],row[6],idx, ))
		location_jobs.append(t)
	for t in location_jobs:
		t.start()
	for t in location_jobs:
		t.join()
		
	if redo_list:
		print "DO OVER!"
		new_redo_list = redo_list
		redo_list = []
		for t in new_redo_list:
			time.sleep(5)
			t.start()
		for t in new_redo_list:
			t.join()
	
	print 'Lenght of redo_list = %d' %len(redo_list)
		
	
	print 'LENGTH: %s' % len(locationDict)
	for row in mycsv[1:]:

		ts = datetime.datetime.strptime(row[3][:25] , '%a, %b/%d/%Y %H:%M:%S').replace(tzinfo=from_zone)	# Fri, Oct/27/2017 16:14:15 (UTC) -> Fri, Oct/27/2017 16:14:15
		
		# Convert time zone
		ts = ts.astimezone(to_zone)
		ts = ts.replace(tzinfo=None)
		try:
			zombie_days = (rightNow - ts).days
		except:
			tb = sys.exc_info()[2]
			tbinfo = traceback.format_tb(tb)[0]
			pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
			print pymsg
			print 'rightNow: %s' %str(rightNow)
			print 'date: %s' % row[3][:25]
			print 'SN: %s' %row[8]
		if (zombie_days < 2) and (row[8] not in retestList):
			#add to list and prepare html
			recentNoTest_list[row[5]].append([row[8],row[9],row[6],zombie_days,locationDict[row[8]],noteDict[row[8]]])
			
		elif (zombie_days > 2) and (row[8] not in retestList): # These have not been retested in 2 days
			# add to list and write to file
			agingNoTest_list[row[5]].append([row[8],row[9],row[6],zombie_days,locationDict[row[8]],noteDict[row[8]]])	#Product: [SN,link,test,days]

	if recentNoTest_list or agingNoTest_list:
		if recentNoTest_list:
			for product in recentNoTest_list:
				recentNoTest_list[product].sort(key=lambda x: int(x[3]), reverse=True)
			
				aging_html = '<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR LESS THAN 2 DAYS:</b></p>\n\
							<table style="border: 1px solid black ;border-collapse:collapse;">\n\
								<tr style="background-color: #CFCFA3;"> \n\
									<th style="border: 1px solid black ;">Product</th>\n\
									<th style="border: 1px solid black ;">Quantity</th>\n\
									<th style="border: 1px solid black ;">Serial Number</th>\n\
									<th style="border: 1px solid black ;">Test</th>\n\
									<th style="border: 1px solid black ;">Age (Days)</th>\n\
									<th style="border: 1px solid black ;">Current Location</th>\n\
									<th style="border: 1px solid black ;">Last Note</th>\n\
								</tr>'
				color_switch=0
				for item in sorted(recentNoTest_list, key=lambda k: len(recentNoTest_list[k]), reverse=True):
					rowsToSpan=len(recentNoTest_list[item])
					if color_switch == 0:
						aging_html = aging_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
						aging_html = aging_html + '<td style="background-color: #FFFFDB;border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,rowsToSpan)
					else:
						aging_html = aging_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
						aging_html = aging_html + '<td style="background-color: #CFCFA3;border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,rowsToSpan)
					
					for idx, row in enumerate(recentNoTest_list[item]):	#[SN,link,test,days]
						mysn=row[0]
						if idx == 0:
							if color_switch == 0:
								aging_html = aging_html + '<td style="background-color: #FFFFDB;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[1],row[0])
							else:
								aging_html = aging_html + '<td style="background-color: #CFCFA3;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[1],row[0])
						else:
							if color_switch == 0:
								aging_html = aging_html + '<tr style="background-color: #FFFFDB;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[1],row[0])
							else:
								aging_html = aging_html + '<tr style="background-color: #CFCFA3;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[1],row[0])
						for indx, ele in enumerate(row[2:]):

							aging_html = aging_html + '<td style="border: 1px solid black ;">%s</td>\n' % ele	#.decode('UTF-8').encode('UTF-8')

						aging_html = aging_html + "</tr>\n"
					if color_switch == 0:
						color_switch += 1
					else:
						color_switch -= 1
				aging_html = aging_html + "</table>\n"
				if not agingNoTest_list:
					aging_html = aging_html + "</html>\n"
					return aging_html
		else:
			aging_html = '<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR LESS THAN 2 DAYS: None</b></p>\n'
			
		if agingNoTest_list:
			for product in agingNoTest_list:
				agingNoTest_list[product].sort(key=lambda x: int(x[3]), reverse=True)
			
				aging_html = aging_html + '<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR MORE THAN 2 DAYS:</b></p>\n\
							<table style="border: 1px solid black ;border-collapse:collapse;">\n\
								<tr style="background-color: #CFCFA3;"> \n\
									<th style="border: 1px solid black ;">Product</th>\n\
									<th style="border: 1px solid black ;">Quantity</th>\n\
									<th style="border: 1px solid black ;">Serial Number</th>\n\
									<th style="border: 1px solid black ;">Test</th>\n\
									<th style="border: 1px solid black ;">Age (Days)</th>\n\
									<th style="border: 1px solid black ;">Current Location</th>\n\
									<th style="border: 1px solid black ;">Last Note</th>\n\
								</tr>'
				color_switch=0
				for item in sorted(agingNoTest_list, key=lambda k: len(agingNoTest_list[k]), reverse=True):
					rowsToSpan=len(agingNoTest_list[item])
					if color_switch == 0:
						aging_html = aging_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
						aging_html = aging_html + '<td style="background-color: #FFFFDB;border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,rowsToSpan)
					else:
						aging_html = aging_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,item)
						aging_html = aging_html + '<td style="background-color: #CFCFA3;border: 1px solid black ;"rowspan="%s">%s</td>\n' % (rowsToSpan,rowsToSpan)
					
					for idx, row in enumerate(agingNoTest_list[item]):	#[SN,link,test,days]
						mysn=row[0]
						if idx == 0:
							if color_switch == 0:
								aging_html = aging_html + '<td style="background-color: #FFFFDB;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[1],row[0])
							else:
								aging_html = aging_html + '<td style="background-color: #CFCFA3;border: 1px solid black ;"><a href="%s">%s</a>\n' % (row[1],row[0])
						else:
							if color_switch == 0:
								aging_html = aging_html + '<tr style="background-color: #FFFFDB;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[1],row[0])
							else:
								aging_html = aging_html + '<tr style="background-color: #CFCFA3;border: 1px solid black ;"><td><a href="%s">%s</a>\n' % (row[1],row[0])
						for indx, ele in enumerate(row[2:]):

							aging_html = aging_html + '<td style="border: 1px solid black ;">%s</td>\n' % ele	#.decode('UTF-8').encode('UTF-8')

						aging_html = aging_html + "</tr>\n"
					if color_switch == 0:
						color_switch += 1
					else:
						color_switch -= 1
				aging_html = aging_html + "</table></html>\n"
				return aging_html
		else:
			aging_html = '<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR MORE THAN 2 DAYS: None</b></p>\n'
	else:
		aging_html = '<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR LESS THAN 2 DAYS: None</b></p>\n\
		<br><b><p style="font-size:15px">FAILURES NOT RETESTED FOR MORE THAN 2 DAYS: None</b></p>\n\
		</html>\n'
		return aging_html


# Check if Vulcan has updated and DL new CSV
## Add try/except instead of using variable
getVulcanCSV_api()
print 'sucess= %d' %sucess
if sucess == 0:
	getVulcanCSV_selenium()


# Compare New csv with data from last update, Start html generation
VulcNum,body,noBAD=getVulcNum()

# Get number of units retested since last update, write operator errors to file, create html to list the test failures for this update only
more_data = getInfo()

# Create html tables for units that have not been retested in under 2 days since test fail and units that that havent been retested in more than 2 days since test fail 
aging_fails = failureTracker()	

# Combine html
if more_data:
	body= body + more_data + aging_fails.encode('utf-8')
else: 
	body= body + aging_fails.encode('utf-8')

# Send Email
print 'Sending Vulcan Number Email'
emailit(VulcNum,body)

# Run Daily failure report
print 'Sending Recovery Email'
#os.system(failureScript)


		